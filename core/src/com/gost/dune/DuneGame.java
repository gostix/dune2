package com.gost.dune;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gost.screens.DuneMain;

/**
 * The type Dune game.
 */
public class DuneGame extends Game {
    /**
     * The Batch.
     */
    public SpriteBatch batch;

    @Override
    public void create() {
        batch = new SpriteBatch();
        setScreen(new DuneMain(this));
    }

    @Override
    public void setScreen(Screen screen) {
        DuneState.getState().setScreen(screen);
        super.setScreen(screen);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
