package com.gost.dune;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.gost.level.UnitsMatrix;

public class DuneState {


    private static DuneState instance;
    private Screen stage;
    private TiledMapTileLayer terrainLayer;
    private UnitsMatrix unitsMatrix;

    public static DuneState getState() {
        if (instance == null) {
            instance = new DuneState();
        }
        return instance;
    }

    private DuneState() {
    }

    public void setScreen(Screen stage) {
        this.stage = stage;
    }

    public Screen getStage() {
        return stage;
    }

    public void setTerrainLayer(TiledMapTileLayer terrainLayer) {
        this.terrainLayer = terrainLayer;
    }

    public UnitsMatrix getUnitsMatrix() {
        return unitsMatrix;
    }

    public void setUnitsMatrix(UnitsMatrix unitsMatrix) {
        this.unitsMatrix = unitsMatrix;
    }

    public TiledMapTileLayer getTerrainLayer() {
        return terrainLayer;
    }
}
