package com.gost.level;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Set;

import com.badlogic.gdx.graphics.Pixmap;
import com.gost.actors.Unit;

/**
 * The type Units matrix.
 */
public class UnitsMatrix {

    private Unit[][] unitsMatrix;

    public UnitsMatrix(int lenght, int height) {
        unitsMatrix = new Unit[lenght][height];
    }

    /**
     * Add unit.
     *
     * @param units the units
     */
    public void addUnit(Unit... units) {
        for (Unit unit : units) {
            if (unitsMatrix[unit.getGridX()][unitsMatrix[0].length - 1 - unit.getGridY()] == null) {
                unitsMatrix[unit.getGridX()][unitsMatrix[0].length - 1 - unit.getGridY()] = unit;
            }
        }
    }


    /**
     * Add unit.
     *
     * @param units the units
     */
    public void addUnit(Set<? extends Unit> units) {

    }

    public boolean shiftUnit(Unit unit, int destinationX, int destinationY) {
        int currentX = unit.getGridX();
        int currentY = unit.getGridY();
        Unit unitInMatrix = unitsMatrix[currentX][unitsMatrix[0].length - 1 - currentY];
        if (unitInMatrix != null) {
            if (unitInMatrix == unit) {
                unitsMatrix[currentX][unitsMatrix[0].length - 1 - currentY] = null;
                unitsMatrix[destinationX][unitsMatrix[0].length - 1 - destinationY] = unit;
                return true;
            } else {
                throw new NoSuchElementException("Only one unit can be in cell");
            }
        }
        return false;
    }

    public Unit getUnit(int x, int y) {
        return unitsMatrix[x][y];
    }
}
