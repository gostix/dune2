package com.gost.level;

import com.gost.actors.Structure;

/**
 * The type Structures matrix.
 */
public class StructuresMatrix {
    /**
     * The Structures matrix.
     */
    Structure[][] structuresMatrix;

    /**
     * Instantiates a new Structures matrix.
     *
     * @param width  the width
     * @param height the height
     */
    public StructuresMatrix(int width, int height) {
        structuresMatrix = new Structure[width][height];
    }

    /**
     * Place structure boolean.
     *
     * @param x         the x
     * @param y         the y
     * @param structure the structure
     * @return the boolean
     */
    public boolean placeStructure(int x, int y, Structure structure) {
        structure.setSourceBlock(x, y);
        for (int i = x; i < i + structure.getWidth(); i++) {
            for (int j = y; i < i + structure.getHeight(); j++) {
                structuresMatrix[i][j] = structure;
            }
        }
        return true;
    }

    /**
     * Destroy structure.
     *
     * @param x the x
     * @param y the y
     */
    public void destroyStructure(int x, int y) {
        Structure structure = structuresMatrix[x][y];
        for (int i = structure.getSourceX(); i < i + structure.getWidth(); i++) {
            for (int j = structure.getSourceY(); i < i + structure.getHeight(); j++) {
                structuresMatrix[i][j] = null;
            }
        }
    }

    /**
     * Gets structure.
     *
     * @param x the x
     * @param y the y
     * @return the structure
     */
    public Structure getStructure(int x, int y) {
        return structuresMatrix[x][y];
    }
}
