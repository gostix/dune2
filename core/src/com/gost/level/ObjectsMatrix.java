package com.gost.level;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.gost.dune.DuneState;

/**
 * The type Objects matrix.
 */
public class ObjectsMatrix {
    public static final float TILE_SIZE = 32f;
    private boolean[][] objectsMatrix;
    private static ObjectsMatrix instance;

    public static ObjectsMatrix getMatrix() {
        if (instance == null) {
            instance = new ObjectsMatrix();
        }
        return instance;
    }

    private ObjectsMatrix() {
        TiledMapTileLayer getTerrainLayer = DuneState.getState().getTerrainLayer();
        objectsMatrix = new boolean[getTerrainLayer.getWidth()][getTerrainLayer.getHeight()];
    }
}
