package com.gost.level;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gost.actors.Unit;

/**
 * The type Dune stage.
 */
public class DuneStage extends Stage {
    private Array<Unit> units;

    /**
     * Instantiates a new Dune stage.
     *
     * @param viewport the viewport
     * @param batch    the batch
     */
    public DuneStage(Viewport viewport, SpriteBatch batch) {
        super(viewport, batch);
    }

    /**
     * Gets units.
     *
     * @return the units
     */
    public Array<Actor> getUnits() {
        // TODO Auto-generated method stub
        return super.getActors();
    }

    @Override
    public Array<Actor> getActors() {
        // TODO Auto-generated method stub
        return super.getActors();
    }

    /**
     * Add actor.
     *
     * @param unit the unit
     */
    public void addActor(Unit unit) {
        // TODO Auto-generated method stub

    }
}
