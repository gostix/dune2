package com.gost.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gost.actors.ObjectsMap;
import com.gost.actors.Trike;
import com.gost.controller.InputHandler;
import com.gost.dune.DuneGame;
import com.gost.dune.DuneState;
import com.gost.level.ObjectsMatrix;
import com.gost.level.UnitsMatrix;
import com.gost.scenes.Hud;
import com.gost.scenes.TiledLayerCamera;

/**
 * The type Dune main.
 */
public class DuneMain implements Screen {

    private DuneGame game;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private TiledLayerCamera camera;
    public SpriteBatch batch;
    private Viewport viewport;
    private Hud hud;
    private Stage stage;
    private TiledMapTileLayer terrainLayer;
    private InputHandler inputHandler;
    private DuneState state;

    /**
     * Instantiates a new Dune main.
     *
     * @param game the game
     */
    public DuneMain(DuneGame game) {
        this.game = game;
        hud = new Hud(game.batch);
        batch = game.batch;
        state = DuneState.getState();
    }


    @Override
    public void show() {
//		Sound mp3Sound = Gdx.audio.newSound(Gdx.files.internal("sound/CommandPost.mp3"));
//		mp3Sound.loop();
        map = new TmxMapLoader().load(Gdx.files.internal("maps/map.tmx").path());

        renderer = new OrthogonalTiledMapRenderer(map);

        terrainLayer = (TiledMapTileLayer) map.getLayers().get(0);
        DuneState.getState().setTerrainLayer(terrainLayer);
        camera = new TiledLayerCamera(terrainLayer);
        float y = terrainLayer.getHeight() * terrainLayer.getTileHeight() - Gdx.graphics.getHeight() / 2f;
        camera.position.set(Gdx.graphics.getWidth() / 2f, y, 0);
        renderer.setView(camera);
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera);
        stage = new Stage(viewport, batch) {
            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return super.touchUp(screenX, screenY, pointer, button);
            }
        };
        state.setUnitsMatrix(new UnitsMatrix(terrainLayer.getWidth(), terrainLayer.getHeight()));

        Trike trike1 = new Trike(3, 8);
        Trike trike2 = new Trike(5, 10);
        Trike trike3 = new Trike(8, 7);
        Trike trike4 = new Trike(9, 9);

        stage.addActor(trike1);
        stage.addActor(trike2);
        stage.addActor(trike3);
        stage.addActor(trike4);

        ObjectsMap objectsMap = new ObjectsMap(terrainLayer.getWidth(), terrainLayer.getHeight());
        objectsMap.setPosition(480, 32);
        hud.getStage().addActor(objectsMap);

        Gdx.input.setInputProcessor(stage);
        inputHandler = new InputHandler(stage, camera);
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        game.batch.setProjectionMatrix(hud.getStage().getCamera().combined);
        renderer.setView(camera);
        renderer.render();
        hud.draw();
        stage.draw();
//        try {
//           Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    /**
     * Update.
     *
     * @param delta the delta
     */
    public void update(float delta) {
        inputHandler.handleInput(delta);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    public TiledMapTileLayer getTerrainLayer() {
        return terrainLayer;
    }
}
