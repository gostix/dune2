package com.gost.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.graphics.Color;

/**
 * The type Hud.
 */
public class Hud {
    private Stage stage;
    private Viewport viewport;
    private Integer credits = 0;
    /**
     * The Credits label.
     */
    public Label creditsLabel;
    /**
     * The No money label.
     */
    public Label noMoneyLabel;

    public Hud(SpriteBatch sb) {
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), new OrthographicCamera());
        stage = new Stage(viewport, sb);

        Table table = new Table();
        table.top();
        table.setFillParent(true);

        creditsLabel = new Label(credits.toString(), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        noMoneyLabel = new Label("CREDITS LOW", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        table.add(creditsLabel).expandX().right().padRight(8f).padTop(10f);
        table.row();
        table.add(noMoneyLabel).center().padTop(Gdx.graphics.getHeight() / 2);
        noMoneyLabel.setVisible(false);
        stage.addActor(table);
    }

    public void draw() {
        creditsLabel.setText(credits.toString());
        stage.draw();
    }

    public Stage getStage() {
        return stage;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getCredits() {
        return credits;
    }

    //public boolean add

}
