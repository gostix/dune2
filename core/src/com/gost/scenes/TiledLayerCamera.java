package com.gost.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

/**
 * The type Tiled layer camera.
 */
public class TiledLayerCamera extends OrthographicCamera {
    private TiledMapTileLayer mapLayer;
    private float topBorder;
    private float bottomBorder;
    private float leftBorder;
    private float rightBorder;

    /**
     * Instantiates a new Tiled layer camera.
     *
     * @param mapLayer the map layer
     */
    public TiledLayerCamera(TiledMapTileLayer mapLayer) {
        this.mapLayer = mapLayer;
        topBorder = Gdx.graphics.getHeight() / 2;
        bottomBorder = mapLayer.getHeight() * mapLayer.getTileHeight() - Gdx.graphics.getHeight() / 2f;
        rightBorder = mapLayer.getWidth() * mapLayer.getTileWidth() - Gdx.graphics.getWidth() / 2f;
        leftBorder = Gdx.graphics.getWidth() / 2;
    }

    /**
     * Move x.
     *
     * @param units the units
     */
    public void moveX(float units) {
        if (units < 0) {
            if (rightBorder <= position.x - units) {
                position.x = rightBorder;
                return;
            }
            position.x -= units;
        } else if (units > 0) {
            if (leftBorder >= position.x - units) {
                position.x = leftBorder;
                return;
            }
            position.x -= units;
        }

    }

    /**
     * Move y.
     *
     * @param units the units
     */
    public void moveY(float units) {
        if (units < 0) {
            if (topBorder >= position.y + units) {
                position.y = topBorder;
                return;
            }
            position.y += units;
        } else if (units > 0) {
            if (bottomBorder <= position.y + units) {
                position.y = bottomBorder;
                return;
            }
            position.y += units;
        }
    }


}
