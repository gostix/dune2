package com.gost.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.gost.actors.Unit;
import com.gost.level.ObjectsMatrix;
import com.gost.scenes.TiledLayerCamera;

/**
 * The type Input handler.
 */
public class InputHandler {
    private Stage stage;
    private TiledLayerCamera camera;
    private boolean touched;
    boolean isSelected = false;
    boolean isDragged;

    /**
     * Instantiates a new Input handler.
     *
     * @param stage  the stage
     * @param camera the camera
     */
    public InputHandler(Stage stage, TiledLayerCamera camera) {
        this.stage = stage;
        this.camera = camera;

        Gdx.input.setInputProcessor(new MyInputProcessor());
    }

    /**
     * Handle input.
     *
     * @param delta the delta
     */
    public void handleInput(float delta) {
//		if (Gdx.input.isTouched())
//		{
//			camera.moveX(Gdx.input.getDeltaX());
//			camera.moveY(Gdx.input.getDeltaY());
//
//
////			if (Gdx.input.justTouched())
////			{
////				for (Actor actor : stage.getActors())
////				{
////					if (actor.getListeners().size > 0)
////					{
////						ClickListener cl = (ClickListener) actor.getListeners().first();
////						if (cl.isPressed())
////						{
////							ActorSelecter.getSelector().select(actor);
////							break;
////						}
////					}
////				}
////			}
//		}
////		if (stage.touchUp(Gdx.input.getX(), Gdx.input.getY(), 0, 0))
////		{
////			System.out.println("PP");
////		}
////}

        camera.update();


    }

    public class MyInputProcessor implements InputProcessor {
        @Override
        public boolean keyDown(int keycode) {
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return stage.touchDown(screenX, screenY, pointer, button);
        }

        @Override
        public boolean touchUp(int x, int y, int pointer, int button) {

            if (!stage.touchUp(x, y, pointer, button) && TouchListener.touched && !isDragged) {
                Vector3 cameraPosition = camera.unproject(new Vector3(x, y, 0));
                float worldX = cameraPosition.x - ObjectsMatrix.TILE_SIZE / 2;
                float worldY = cameraPosition.y - ObjectsMatrix.TILE_SIZE / 2;
                Unit unit = (Unit) ActorSelector.getSelector().getActor();
                unit.moveToGrid(Math.round(worldX / ObjectsMatrix.TILE_SIZE), Math.round(worldY / ObjectsMatrix.TILE_SIZE));
                ActorSelector.getSelector().release();
                TouchListener.touched = false;
            }
            isDragged = false;
            return false;
        }

        @Override
        public boolean touchDragged(int x, int y, int pointer) {
            camera.moveX(Gdx.input.getDeltaX());
            camera.moveY(Gdx.input.getDeltaY());
            if (Math.abs(Gdx.input.getDeltaX()) > 4 || Math.abs(Gdx.input.getDeltaY()) > 4) {
                isDragged = true;
            }

            return false;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {

            return false;
        }


        @Override
        public boolean scrolled(int amount) {
            return false;
        }
    }

}