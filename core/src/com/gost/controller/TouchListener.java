package com.gost.controller;


import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class TouchListener extends ClickListener {
    public static boolean touched;
    private boolean isSelected = false;
    private boolean moved;
    private Stage stage;

    public TouchListener(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (inTapSquare()) {
            touched = true;
            ActorSelector.getSelector().select(event.getTarget());
        }

        super.touchUp(event, x, y, pointer, button);
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        super.touchDragged(event, x, y, pointer);
    }
}
