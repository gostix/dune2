package com.gost.controller;

import java.util.Set;

import com.gost.actors.Unit;

/**
 * The type Game frame.
 */
public class GameFrame {
    private static Set<Unit> actors;

    /**
     * Instantiates a new Game frame.
     */
    public GameFrame() {

    }

    /**
     * Add actors.
     *
     * @param unit the unit
     */
    public void addActors(Unit... unit) {

    }

    /**
     * Remove actors.
     *
     * @param unit the unit
     */
    public void removeActors(Unit... unit) {

    }

    /**
     * Calculate units position.
     */
    public void calculateUnitsPosition() {

    }


}
