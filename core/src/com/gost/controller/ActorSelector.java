package com.gost.controller;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ActorSelector {

    private Actor unitSelector;
    private static ActorSelector instance;
    private Actor selectedActor;

    private ActorSelector() {
        unitSelector = new Actor() {
            int visibilityCounter = 1;
            boolean isVisible = true;
            final Texture clickedTexture = new Texture(Gdx.files.internal("drawable/clicked.png"));
            final Sprite sprite = new Sprite(clickedTexture);

            @Override
            public void setVisible(boolean visible) {
                super.setVisible(visible);
                isVisible = visible;
            }

            @Override
            public void draw(Batch batch, float parentAlpha) {
                if (visibilityCounter++ % 15 == 0) {
                    isVisible = !isVisible;
                    visibilityCounter = 1;
                }
                if (isVisible) {
                    sprite.setPosition(selectedActor.getX(), selectedActor.getY());
                    sprite.setSize(clickedTexture.getWidth(), clickedTexture.getHeight());
                    sprite.draw(batch);
                }
            }
        };
    }

    public void select(Actor actor) {
        selectedActor = actor;
        unitSelector.setVisible(true);
        unitSelector.setPosition(selectedActor.getX(), selectedActor.getY());
        selectedActor.getStage().addActor(unitSelector);
    }

    public void release() {
        unitSelector.setVisible(false);
        selectedActor = null;
    }

    public Actor getActor() {
        return selectedActor;
    }

    public static ActorSelector getSelector() {
        if (instance == null) {
            instance = new ActorSelector();
        }
        return instance;
    }
}
