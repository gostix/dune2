package com.gost.actors;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.gost.controller.TouchListener;

public class Trike extends Unit {

    /**
     * Instantiates a new Unit.
     *
     * @param gridX the grid x
     * @param gridY the grid y
     */
    public Trike(int gridX, int gridY) {
        super(gridX, gridY);
        texture = new Texture(Gdx.files.internal("drawable/trike_t.png"));
        sprite = new Sprite(texture);
        TouchListener tl = new TouchListener(getStage());
        tl.setTapSquareSize(32f);
        addListener(tl);
    }

    @Override
    public void setHealth(int points) {

    }

    @Override
    public void takeDemage(int points) {

    }

    public boolean touchDown(float x, float y, int pointer) {
        return false;
    }
}
