package com.gost.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gost.dune.DuneState;
import com.gost.level.ObjectsMatrix;
import com.gost.level.UnitsMatrix;

/**
 * The type Unit.
 */
public abstract class Unit extends Actor {
    protected Texture texture;

    protected float speed = 1;
    protected int gridX;
    protected int gridY;
    protected int gridXDestination;
    protected int gridYDestination;
    protected float health;
    protected boolean isMove;
    protected Sprite sprite;
    protected boolean isRotate;
    protected Side currentSide;
    protected float unitRotation;
    protected int nextGridX;
    protected int nextGridY;
    protected boolean canInterupt;
    private UnitsMatrix unitsMatrix;

    /**
     * Instantiates a new Unit.
     *
     * @param gridX the grid x
     * @param gridY the grid y
     */
    public Unit(int gridX, int gridY) {
        this.gridX = gridX;
        this.gridY = gridY;
        setX(gridX * ObjectsMatrix.TILE_SIZE);
        setY(gridY * ObjectsMatrix.TILE_SIZE);
        setHeight(ObjectsMatrix.TILE_SIZE);
        setWidth(ObjectsMatrix.TILE_SIZE);
        unitRotation = 0f;
        unitsMatrix = DuneState.getState().getUnitsMatrix();
        unitsMatrix.addUnit(this);
        debug();
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        checkPosition();

        sprite.setPosition(getX(), getY());
        sprite.setSize(texture.getWidth(), texture.getHeight());
        sprite.setRotation(-unitRotation);
        sprite.draw(batch);
    }

    protected boolean isNextCellFree()
    {

        return false;
    }

    /**
     * Check position.
     */
    protected void checkPosition() {
        if (isMove) {
            boolean stopX = false;
            boolean stopY = false;
            currentSide = calculateSide();

            rotate();
            if (!isRotate) {
                if (getX() != gridXDestination * ObjectsMatrix.TILE_SIZE) {
                    if (getX() - (gridXDestination * ObjectsMatrix.TILE_SIZE) > 0) {
                        setX(getX() - speed);
                    } else {
                        setX(getX() + speed);
                    }
                    int newGridX = (int) (getX() / ObjectsMatrix.TILE_SIZE);
                    if (newGridX != gridX) {
                        unitsMatrix.shiftUnit(this, newGridX, gridY);
                        gridX = newGridX;
                    }
                } else {
                    stopX = true;
                }
                if (getY() != gridYDestination * ObjectsMatrix.TILE_SIZE) {
                    if (getY() - (gridYDestination * ObjectsMatrix.TILE_SIZE) > 0) {
                        setY(getY() - speed);
                    } else {
                        setY(getY() + speed);
                    }
                    int newGridY = (int) (getY() / ObjectsMatrix.TILE_SIZE);
                    if (newGridY != gridY) {
                        unitsMatrix.shiftUnit(this, gridX, newGridY);
                        gridY = newGridY;
                    }
                } else {
                    stopY = true;
                }

                if (stopX && stopY) {
                    isMove = false;
                }
            }
        }
    }

    @Override
    public void setX(float x) {
        super.setX(x);
    }

    public Side getCourse() {

        return null;
    }

    /**
     * Move to grid.
     *
     * @param x the x
     * @param y the y
     */
    public void moveToGrid(int x, int y) {
        isMove = true;
        gridXDestination = x;
        gridYDestination = y;
    }

    enum Side {
        N(0), NE(45), E(90), SE(135), S(180), SW(225), W(270), NW(315);

        protected float degrees;

        Side(float degrees) {
            this.degrees = degrees;
        }

        public float getDegrees() {
            return degrees;
        }
    }

    protected Side calculateSide() {
        float deltaX = getX() - (gridXDestination * ObjectsMatrix.TILE_SIZE);
        float deltaY = getY() - (gridYDestination * ObjectsMatrix.TILE_SIZE);

        if (deltaX == 0) {
            if (deltaY < 0) {
                return Side.N;
            } else if (deltaY > 0) {
                return Side.S;
            }
        } else if (deltaY == 0) {
            if (deltaX < 0) {
                return Side.E;
            } else if (deltaX > 0) {
                return Side.W;
            }
        } else if (deltaX < 0) {
            if (deltaY < 0) {
                return Side.NE;
            } else if (deltaY > 0) {
                return Side.SE;
            }
        } else {
            if (deltaY < 0) {
                return Side.NW;
            } else if (deltaY > 0) {
                return Side.SW;
            }
        }
        return currentSide;
    }

    public void rotateUnitBy(float amountInDegrees) {
        isRotate = true;
        unitRotation += amountInDegrees;
        if (unitRotation >= 360f) {
            unitRotation -= 360f;
        } else if (unitRotation < 0f) {
            unitRotation += 360f;
        }

    }

    private void rotate() {
        if (currentSide.getDegrees() == unitRotation) {
            isRotate = false;
            return;
        }
        float difference = currentSide.getDegrees() - unitRotation;
        if (difference < 0 && Math.abs(difference) < 180 || difference > 0 && Math.abs(difference) > 180) {
            rotateUnitBy(-3f);
        } else {
            rotateUnitBy(3f);
        }
    }

    /**
     * Gets grid x.
     *
     * @return the grid x
     */
    public int getGridX() {
        return gridX;
    }

    /**
     * Gets grid y.
     *
     * @return the grid y
     */
    public int getGridY() {
        return gridY;
    }

    /**
     * Move to.
     *
     * @param x the x
     * @param y the y
     */
    public void moveTo(int x, int y) {
        gridXDestination = x;
        gridYDestination = y;
    }

    /**
     * Sets health.
     *
     * @param points the points
     */
    public abstract void setHealth(int points);

    /**
     * Take demage.
     *
     * @param points the points
     */
    public abstract void takeDemage(int points);

    /**
     * Is alive boolean.
     *
     * @return the boolean
     */
    public boolean isAlive() {
        return false;
    }

    /**
     * Gets health.
     *
     * @return the health
     */
    public float getHealth() {
        return health;
    }

    /**
     * Gets position x.
     *
     * @return the position x
     */
    public int getPositionX() {
        return 0;
    }

    /**
     * Gets position y.
     *
     * @return the position y
     */
    public int getPositionY() {
        return 0;
    }

    /**
     * Sets commander.
     *
     * @param commander the commander
     */
    public void setCommander(byte commander) {
    }

    /**
     * Shoot weapon.
     *
     * @return the weapon
     */
    public Weapon shoot() {
        return null;
    }

}
