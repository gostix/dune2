package com.gost.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gost.dune.DuneState;
import com.gost.level.UnitsMatrix;

import java.util.Random;


public class ObjectsMap extends Actor {

    private Pixmap pixmap;
    private Pixmap bordersPixmap;
    private Texture texture;
    private Sprite map;
    private int positionX;
    private int positionY;
    private int width;
    private int height;

    public ObjectsMap(int width, int height) {
        this.width = width;
        this.height = height;
        pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        bordersPixmap = new Pixmap(width + 2, height + 2, Pixmap.Format.RGBA8888);
        texture = new Texture(bordersPixmap);
        map = new Sprite(texture);
        map.setSize(136, 136);
    }

    private void drawBorders(Color color) {
        bordersPixmap.setColor(color);
        bordersPixmap.drawLine(0, 0, 0, height + 1);
        bordersPixmap.drawLine(0, 0, width + 1, 0);
        bordersPixmap.drawLine(width + 1, 0, width + 1, height + 1);
        bordersPixmap.drawLine(width + 1, height + 1, 0, height + 1);
    }

    private void reBuildMap() {
        bordersPixmap.setColor(Color.BLACK);
        bordersPixmap.fill();
        pixmap.fill();
        drawBorders(Color.BLUE);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (DuneState.getState().getUnitsMatrix().getUnit(x, y) != null) {
                    pixmap.drawPixel(x, y, Color.RED.toIntBits());
                }

            }
        }
        bordersPixmap.drawPixmap(pixmap, 1, 1);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        reBuildMap();
        texture.draw(bordersPixmap, 0, 0);
        map.setPosition(positionX, positionY);
        map.draw(batch);
    }

    public void setPosition(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }
}
