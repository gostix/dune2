package com.gost.actors;

import com.gost.screens.Menu;

/**
 * The type Structure.
 */
public abstract class Structure {
    // enum Structures
    // {
    // CONCRETE(2, 2),
    // WINDTRAP(2, 2),
    // REFINERY(3, 2),
    // OUTPOST(2, 2),
    // SPICE_SILO(2, 2),
    // VEHICLE(3, 2),
    // BARRAKS(2, 2),
    // WALL(1, 1),
    // TURRET(1, 1),
    // R_TURRET(1, 1),
    // REPAIR(3, 2),
    // HI_TECH(2, 2),
    // STARPORT(3, 3),
    // PALACE(3, 3);
    //
    //
    // }


    /**
     * The X.
     */
    int x;
    /**
     * The Y.
     */
    int y;
    /**
     * The Health.
     */
    int health;

    /**
     * Gets width.
     *
     * @return the width
     */
    public abstract int getWidth();

    /**
     * Gets height.
     *
     * @return the height
     */
    public abstract int getHeight();

    /**
     * Open menu menu.
     *
     * @return the menu
     */
    public abstract Menu openMenu();

    /**
     * Gets energy.
     *
     * @return the energy
     */
    public abstract int getEnergy();

    /**
     * Gets cost.
     *
     * @return the cost
     */
    public abstract int getCost();

    /**
     * Sets health.
     *
     * @param health the health
     */
    public abstract void setHealth(int health);

    /**
     * Gets name.
     *
     * @return the name
     */
    public abstract String getName();

    /**
     * Sets source block.
     *
     * @param x the x
     * @param y the y
     */
    public void setSourceBlock(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets source x.
     *
     * @return the source x
     */
    public int getSourceX() {
        return x;
    }

    /**
     * Gets source y.
     *
     * @return the source y
     */
    public int getSourceY() {
        return y;
    }

    /**
     * Gets health.
     *
     * @return the health
     */
    public int getHealth() {
        return health;
    }

}
