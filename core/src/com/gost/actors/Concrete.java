package com.gost.actors;

import com.gost.screens.Menu;

/**
 * The type Concrete.
 */
public class Concrete extends Structure {

    private static final int HEIGHT = 2;
    private static final int WIDTH = 2;

    @Override
    public int getWidth() {
        return WIDTH;
    }

    @Override
    public int getHeight() {
        return HEIGHT;
    }

    @Override
    public Menu openMenu() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getEnergy() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getCost() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setHealth(int health) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }
}
