package com.gost.dune.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gost.dune.DuneGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "DUNE 2";
		config.useGL30 = true;
		config.width = 640;
		config.height = 480;
		new LwjglApplication(new DuneGame(), config);
	}
}
